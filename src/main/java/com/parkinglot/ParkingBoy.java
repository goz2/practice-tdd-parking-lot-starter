package com.parkinglot;

import java.util.*;
import java.util.stream.Collectors;

public class ParkingBoy {

    private ParkingLot parkingLot;

    private List<ParkingLot> parkingLots;

    public ParkingBoy() {
        parkingLots = new ArrayList<>();
    }

    public void addParkingLot(ParkingLot ...parkingLot) {
        parkingLots.addAll(Arrays.asList(parkingLot));
    }

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFUll())
                .findFirst()
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isIn(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedException::new);
    }
}
