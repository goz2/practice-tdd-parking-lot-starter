package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private final Map<Ticket, Car> map = new HashMap<>();
    private int capacity = 10;

    public ParkingLot() {
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean isIn(Ticket ticket) {
        return map.containsKey(ticket);
    }
    public boolean isFUll() {
        return map.size() == capacity;
    }

    public Ticket park(Car car) {
        if (map.size() >= capacity) {
            throw new NoAvailablePositionException();
        }
        Ticket ticket = new Ticket();
        map.putIfAbsent(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        Car car = map.getOrDefault(ticket, null);
        if (car == null) {
            throw new UnrecognizedException();
        }
        map.remove(ticket);
        return car;
    }
}
