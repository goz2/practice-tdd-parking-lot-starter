package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_car_given_parking_boy_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        for (int i = 0; i < 3; i++) {
            Car car_i = new Car();
            parkingBoy.park(car_i);
        }

        Car car = new Car();

        // when
        Ticket ticket = parkingBoy.park(car);

        // then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_a_ticket() {
        // given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);
        Car car = new Car();
        Ticket parkTicket = parkingBoy.park(car);

        // when
        Car car_ = parkingBoy.fetch(parkTicket);

        // then
        Assertions.assertNotNull(car_);
    }

    @Test
    void should_return_two_car_when_fetch_car_by_parking_boy_given_two_ticket() {
        // given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);

        Car car1 = new Car();
        Car car2 = new Car();
        Ticket parkTicket1 = parkingBoy.park(car1);
        Ticket parkTicket2 = parkingBoy.park(car2);

        // when
        Car car_1 = parkingBoy.fetch(parkTicket1);
        Car car_2 = parkingBoy.fetch(parkTicket2);

        // then
        Assertions.assertNotNull(car_1);
        Assertions.assertNotNull(car_2);
    }

    @Test
    void should_return_nothing_when_fetch_car_by_parking_boy_given_wrong_ticket() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_car_by_parking_boy_given_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);

        Ticket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }

    @Test
    void should_return_nothing_when_park_car_by_parking_boy_given_parking_lot_no_position() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingBoy parkingBoy = new ParkingBoy();
        parkingBoy.addParkingLot(parkingLot1);
        parkingBoy.addParkingLot(parkingLot2);

        for (int i = 0; i < 4; i++) {
            Car car_i = new Car();
            parkingBoy.park(car_i);
        }
        Car car = new Car();

        // when then
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class,
                        () -> parkingBoy.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());
    }
}
