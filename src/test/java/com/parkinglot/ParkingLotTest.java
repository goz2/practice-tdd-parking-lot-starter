package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_car_given_parking_log_a_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        // when
        Ticket ticket = parkingLot.park(car);

        // then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_a_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Ticket parkTicket = parkingLot.park(car);
        Ticket ticket = new Ticket();

        // when
        Car car_ = parkingLot.fetch(parkTicket);

        // then
        Assertions.assertNotNull(car_);
    }

    @Test
    void should_return_two_car_when_fetch_car_given_two_ticket() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket parkTicket1 = parkingLot.park(car1);
        Ticket parkTicket2 = parkingLot.park(car2);

        // when
        Car car_1 = parkingLot.fetch(parkTicket1);
        Car car_2 = parkingLot.fetch(parkTicket2);

        // then
        Assertions.assertNotNull(car_1);
        Assertions.assertNotNull(car_2);
    }

    @Test
    void should_return_nothing_when_fetch_car_given_wrong_ticket() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot = new ParkingLot();

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_car_given_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        Ticket ticket = parkingLot.park(car);
        parkingLot.fetch(ticket);

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }

    @Test
    void should_return_nothing_when_park_car_given_parking_lot_no_position() {
        // given
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 0; i < 10; i++) {
            Car car_i = new Car();
            parkingLot.park(car_i);
        }
        Car car = new Car();

        // when then
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class,
                        () -> parkingLot.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());
    }

}
